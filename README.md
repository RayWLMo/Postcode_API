# Learning more about APIs
## Using a postcode checker API
### First Iteration
For this iteration, the code will tell the user whether the postcode is valid and if the API is working, and relays the status code to the user.
```python
import requests

check_response_postcode = requests.get("https://api.postcodes.io/postcodes/")

if check_response_postcode.status_code == 200:  # If the status code is 200, return True
    print(f"The postcode is valid and returned with status code: {check_response_postcode.status_code}")

else:
    print(f"Something went wrong, the API returned with the status code: {check_response_postcode.status_code}")
```
### Second Iteration
For this iteration, we use a function for the if statement, so it can be called when required. This helps reduce repetitiveness in the code.
```python
import requests

check_response_postcode = requests.get("https://api.postcodes.io/postcodes/")


def Postcode_Check():
    if check_response_postcode:  # All the code and functionality is abstracted from the user
        print(f"success with status code {check_response_postcode.status_code}")
    else:
        print(f"unavailable with status code {check_response_postcode.status_code}")


print(Postcode_Check())
```
### Third Iteration
For this iteration, we use the `headers` function from the `requests` package to read the information about the postcode provided. Using the `type()` function, the dat type of the headers is found to be a dict.
```python
import requests

check_response_postcode = requests.get("https://api.postcodes.io/postcodes/LL185TF")

print(type(check_response_postcode.headers))  # Used to check the type of data the API returns
```
Parsing or extracting the data from the dict
- The `.json()` function is used to convert the data provided into something that can be called and displayed, and we store the dict in the variable `response_dict`.
```py
print(check_response_postcode.json()) 
# Store the data into a variable
response_dict = check_response_postcode.json()
```
We can then use a `for` loop to re-iterate through the dict to print out all the data in the dict.
```py
for key in check_response_postcode:
    print(response_dict)  # Checking the data now to ensure the data can be utilised
```
The data in the dict `response_dict` has 3 keys, `status` (`int`), `result` (`dict`) and `codes` (`dict`). As the only relevant data needed is found in the `result` dict, a new variable is created to store exclusively the information from the `result` dict.
- Using a `for` loop, each key can be printed with its respective value
```py
result_dict = response_dict['result']
print(response_dict)  # Checking if the data is correct
for key in result_dict.keys():
    print(f"The name of the key is {key} and the value inside the key is {result_dict[key]}")
```

### Activity
#### Activity Tasks
- Prompt the user to enter the postcode
- Validate the postcode
- Present the location back to the user with required message
#### Solution
For this task, a function was used so that it can be called repeatedly and avoid repetition and simplifying the code.
A `if` statement was incorporated to filter the results into a valid postcode, invalid postcode and if there's another error unrelated to the user input (e.g., unable to connect to server)
```python
import requests  # importing the requests package


def Postcode_Location():
    user_postcode = input("What postcode would you like to know the location of?  ")
    check_response_postcode = requests.get(f"https://api.postcodes.io/postcodes/{user_postcode}")
    response_dict = check_response_postcode.json()
    if check_response_postcode.status_code == 200:
        result_dict = response_dict['result']
        print("That is a valid postcode\n")
        print(f"Here is the information about {user_postcode.upper()}")
        print(f"The latitude of {user_postcode.upper()} is {result_dict['latitude']}")
        print(f"The longitude of {user_postcode.upper()} is {result_dict['longitude']}")
    else:
        if response_dict['error'] == "Invalid postcode":
            print(f"The postcode entered is invalid. Please try again")
        else:
            return f'Looks like something went wrong. The error given is "{response_dict["error"]}". The status code ' \
               f'provided is: {check_response_postcode.status_code} '


print(Postcode_Location())  # Using the print() function to call the function
```

# # First Iteration
#
# import requests
#
# check_response_postcode = requests.get("https://api.postcodes.io/postcodes/")
#
# if check_response_postcode.status_code == 200:  # If the status code is 200, return True
#     print(f"The postcode is valid and returned with status code: {check_response_postcode.status_code}")
#
# else:
#     print(f"The postcode is invalid, the API returned with the status code: {check_response_postcode.status_code}")
#
# # Second Iteration
#
# import requests
#
# check_response_postcode = requests.get("https://api.postcodes.io/postcodes/")
#
#
# def Postcode_Check():
#     if check_response_postcode:  # All the code and functionality is abstracted from the user
#         print(f"success with status code {check_response_postcode.status_code}")
#     else:
#         print(f"unavailable with status code {check_response_postcode.status_code}")
#
#
# print(Postcode_Check())
# # Research how requests is doing that behind the scenes
# #
# # Third iteration
# import requests
#
# check_response_postcode = requests.get("https://api.postcodes.io/postcodes/LL185TF")
#
# print(type(check_response_postcode.headers))  # Used to check the type of data the API returns
#
# # Parsing or getting the data from the dict
#
# print(check_response_postcode.json())
# # Store the data into a variable
# response_dict = check_response_postcode.json()
#
# for key in check_response_postcode:
#     print(response_dict)  # Checking the data now to ensure the data can be utilised
#
# result_dict = response_dict['result']
# print(response_dict)
# for key in result_dict.keys():
#     print(f"The name of the key is {key} and the value inside the key is {result_dict[key]}")
#
# Prompt the user to enter the postcode
# Validate the postcode
# Present the location back to the user with required message

import requests


def Postcode_Location():
    user_postcode = input("What postcode would you like to know the location of?  ")
    check_response_postcode = requests.get(f"https://api.postcodes.io/postcodes/{user_postcode}")
    response_dict = check_response_postcode.json()
    if check_response_postcode.status_code == 200:
        result_dict = response_dict['result']
        print("That is a valid postcode\n")
        print(f"Here is the information about {user_postcode.upper()}")
        print(f"The latitude of {user_postcode.upper()} is {result_dict['latitude']}")
        print(f"The longitude of {user_postcode.upper()} is {result_dict['longitude']}")
    else:
        if response_dict['error'] == "Invalid postcode":
            print(f"The postcode entered is invalid. Please try again")
        else:
            return f'Looks like something went wrong. The error given is "{response_dict["error"]}". The status code ' \
               f'provided is: {check_response_postcode.status_code} '


print(Postcode_Location())
